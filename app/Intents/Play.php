<?php

namespace App\Intents;

use App\Game;

class Play implements IntentInterface
{
    public function __construct($conversation_id, $text)
    {
        $this->conversation_id = $conversation_id;
        $this->text = $text;
    }

    public function answer()
    {
        if (($game = Game::where(['chat_id' => $this->conversation_id, 'active' => true])->first()) && $game->count() > 0)
        {
            if (!is_numeric($this->text))
                return "Ce n'est pas un nombre :/";
            $text = (int)$this->text;
            if ($game->searching_for == $text)
            {
                $game->active = false;
                $game->save();
                return "BRAVOOOOO !";
            }

            if ($text < $game->searching_for)
            {
                return "Plus grand !";
            }

            return "Plus petit !";
        }


        $game = new Game();
        $game->chat_id = $this->conversation_id;
        $game->moves = '{}';
        $game->searching_for = random_int(0,100);
        $game->save();

        return "Ok allons-y ! J'ai choisi un nombre entre 1 et 100, lequel ?";
    }
}