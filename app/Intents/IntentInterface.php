<?php

namespace App\Intents;

interface IntentInterface
{
    public function answer();
}