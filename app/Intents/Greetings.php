<?php

namespace App\Intents;

class Greetings implements IntentInterface
{

    protected $greetings = ['Salut !', 'Wesh alors poto :)', 'Bonjour à toi !', 'Salut la compagnie !', 'YOOO', 'Coucou hibou !'];

    public function answer()
    {
        return $this->greetings[array_rand($this->greetings)];
    }
}