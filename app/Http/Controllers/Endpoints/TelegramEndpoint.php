<?php
/**
 * Created by PhpStorm.
 * User: Nathanael
 * Date: 28/07/2017
 * Time: 23:50
 */

namespace App\Http\Controllers\Endpoints;

use App\Traits\ProcessesMessage;
use App\Traits\SendsMessage;
use Illuminate\Http\Request;


class TelegramEndpoint extends \App\Http\Controllers\Controller
{
    use ProcessesMessage, SendsMessage;

    public function receiveMessage(Request $request)
    {
        $data = $request->json()->all();

        if (isset($data['message']) && isset($data['message']['text']) && isset($data['message']['date']) && isset($data['message']['chat']['id']))
        {
            $message = $data['message']['text'];
            $conversation_id = $data['message']['chat']['id'];
            $time = $data['message']['date'];

            $answer = $this->processMessage($message, $conversation_id, 'Telegram', date('Y-m-d H:i:s', $time));

            if ($answer != null) {
                return $this->sendMessage($data['message']['chat']['id'], 'Telegram', $answer);
            }
        }
        return response('Cool',200);
    }
}
