<?php

namespace App\Services;

use GuzzleHttp\Client;

abstract class FrontPoint
{
    protected $client;

    /**
     * FrontPoint constructor.
     * @param $authorization
     */
    public function __construct($authorization)
    {
        $this->client = new Client();
    }
}