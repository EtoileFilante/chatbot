<?php

namespace App\Services;

use Illuminate\Support\Facades\Config;

class TelegramFrontPoint extends FrontPoint
{
    public function __construct()
    {
        parent::__construct('Authorization: Token ' . Config::get('services.pat'));
    }

    public function sendMessage($id, $text)
    {
        return $this->client->request("POST", "https://api.telegram.org/bot" . Config::get('services.telegram.pat') . "/sendMessage?chat_id=".$id."&text=".$text);
    }
}
