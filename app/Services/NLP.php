<?php

namespace App\Services;

use Illuminate\Support\Facades\Config;

class NLP extends FrontPoint
{
    private $url;

    private $language;


    public function __construct()
    {
        parent::__construct('Authorization: Token ' . Config::get('services.recast.dev_token'));

        $this->url = 'https://api.recast.ai/v2/request';
        $this->language="fr";
    }

    public function getIntention($text)
    {
        $headers =  ['Authorization'=> 'Token ' . Config::get('services.recast.dev_token')];

        $data = [
            "text" => $text,
            "language" => $this->language,
        ];

        $req = $this->client->request("POST", $this->url, ['headers'=>$headers, 'form_params'=>$data]);
        $result = $req->getBody()->getContents();

        $results = json_decode($result, TRUE)["results"]; // TRUE to get an array object
        if ($results == null) {
            return array("error" => $result['message']);
        }


        if (isset($results['intents'])) {
            foreach ($results['intents'] as &$intent) {
                if (isset($intent['confidence']) && $intent['confidence'] > 0.5) {
                    return $intent['slug'];
                }
            }
        }

        return array('error' => 'Pas d\'intention trouvé');
    }
}
