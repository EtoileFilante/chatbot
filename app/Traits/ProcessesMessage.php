<?php

namespace App\Traits;


use App\Intents\Greetings;
use App\Intents\Play;
use App\Services\NLP;

trait ProcessesMessage
{
    function processMessage($text,$conversation_id,$platform,$time)
    {
        $nlp = new NLP();
        $intention = $nlp->getIntention($text);

        switch ($intention) {
            case 'greetings' :
                $intent = new Greetings();
                break;
            case 'jouer' :
                $intent = new Play($conversation_id, $text);
                break;
            default :
                return null;
                break;
        }

        return $intent->answer();
    }
}