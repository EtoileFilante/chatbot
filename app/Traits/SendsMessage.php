<?php

namespace App\Traits;

use App\Services\TelegramFrontPoint;

trait SendsMessage
{
    function sendMessage($idChat,$platform,$text)
    {
        switch ($platform) {
            case 'Telegram' :
                $frontPoint = new TelegramFrontPoint();
                break;
            default :
                break;
        }

        return $frontPoint->sendMessage($idChat,$text);
    }
}